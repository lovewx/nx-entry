package com.nx.platform.entry.service;

import com.google.gson.JsonObject;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.nx.arch.nxrpc.client.proxy.builder.ProxyFactory;
import com.nx.platform.entry.annotation.scan.AnnotationScan;
import com.nx.platform.entry.utility.Constant;
import com.nx.platform.entry.utility.E2s;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


@Component
@Slf4j
public class DealCommand {
    @Autowired
    AnnotationScan scan;

    @Autowired
    CommondFunction commondFunction;

    private static Map<String, Object> serverCache = new HashMap<>();
    private static Map<String, Method> methodCache = new HashMap<>();

    @HystrixCommand(fallbackMethod = "CommandFallBack",
            commandProperties = {
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "1440"),
                    @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "20")
            })
    public String executeCommand(String command, HttpServletRequest request) {
        String logPre = "command=" + command;

        //网关返回的Json数据
        JsonObject retJson = new JsonObject();
        String retStr = null;

        //获取请求参数
        Map<String, String> paramMap = new HashMap<String, String>();
        Enumeration<String> parameters = request.getParameterNames();
        while (parameters.hasMoreElements()){
            String tmpKey = parameters.nextElement();
            paramMap.put(tmpKey, String.valueOf(request.getParameter(tmpKey)));
        }

        if (!commondFunction.checkParam(paramMap)) {
            log.error(logPre + "err=parameters_error");
            retJson.addProperty(Constant.JSON_RESP_CODE, Constant.RESP_CODE_LEAK_PARAM);
            retJson.addProperty(Constant.JSON_ERROR_MSG, "请求参数非法");
            retStr = retJson.toString();
            return retStr;
        }


        JsonObject confJson = (JsonObject) scan.getCmdMap().get(command);
        //判断请求是否注册
        if (null == confJson || 0 == confJson.size()) {
            log.error(logPre + "no_command_found");
            retJson.addProperty(Constant.JSON_RESP_CODE, Constant.RESP_CODE_LEAK_PARAM);
            retJson.addProperty(Constant.JSON_ERROR_MSG, "请求参数非法");
            retStr = retJson.toString();
            return retStr;
        }

        String serverClass = confJson.get(AnnotationScan.DEFAULT_RPCCLASS_NAME).getAsString();

        Object server = getRpcServer(confJson);  //反射缓存
        Method method = null;
        method = getRpcMethod(confJson, serverClass); //反射缓存
        if (null == method) {
            log.error(logPre + "err=dubbo_reflect_error");
            retJson.addProperty(Constant.JSON_RESP_CODE, Constant.RESP_CODE_LEAK_PARAM);
            retJson.addProperty(Constant.JSON_ERROR_MSG, "服务端错误，请稍后重试");
            retStr = retJson.toString();
            return retStr;
        }

        try {
            retStr = (String) method.invoke(server, paramMap);
        } catch (IllegalAccessException e) {
            log.error(logPre + E2s.exception2String(e));
        } catch (InvocationTargetException e) {
            log.error(logPre + E2s.exception2String(e));
        }
        if (null == retStr) {
            retJson.addProperty(Constant.JSON_RESP_CODE, Constant.RESP_CODE_SERVER_ERROR);
            retJson.addProperty(Constant.JSON_ERROR_MSG, "服务端错误，请稍后重试");
            retStr = retJson.toString();
            return retStr;
        }

        return retStr;
    }

    public String commandFallBack(String command, HttpServletRequest request) {
        //网关返回的Json数据
        JsonObject retJson = new JsonObject();
        String retStr = null;
        retJson.addProperty(Constant.JSON_RESP_CODE, Constant.RESP_CODE_SERVER_ERROR);
        retJson.addProperty(Constant.JSON_ERROR_MSG, "Hystrix fallback: 服务端错误，请稍后重试");
        retStr = retJson.toString();
        return retStr;
    }

    private static Object getRpcServer(JsonObject confJson) {
        String serviceUrl = confJson.get(AnnotationScan.DEFAULT_SERVER_REG_NAME).getAsString();
        if (null == serviceUrl) {
            log.error("desc=scfurl_error name=" + confJson.get("name").getAsString());
            return null;
        }
        String scfClass = confJson.get(AnnotationScan.DEFAULT_RPCCLASS_NAME).getAsString();
        if (null == scfClass) {
            log.error("desc=scfclass_error name=" + confJson.get("name").getAsString());
            return null;
        }

        String mapKey = new StringBuilder(serviceUrl).append("&").append(scfClass).toString();

        Object scfObject = serverCache.get(mapKey);
        if (null == scfObject) {
            try {
                scfObject = ProxyFactory.create(Class.forName(scfClass), serviceUrl);  //获取代理对象
                serverCache.put(mapKey, scfObject); //避免重复初始化
            } catch (ClassNotFoundException e) {
                // TODO: handle exception
                log.error("desc=reflect_class_not_found class=" + scfClass);
                return null;
            }
        }

        return scfObject;
    }

    private static Method getRpcMethod(JsonObject confJson, String scfClass) {

        String scfMethod = confJson.get(AnnotationScan.DEFAULT_RPCMETHOD_NAME).getAsString();
        if (null == scfMethod) {
            log.error("desc=scfmethod_error name=" + confJson.get("name").getAsString());
            return null;
        }
        String mapMethodKey = new StringBuilder().append(scfClass)
                .append("&").append(scfMethod).toString();
        Method method = methodCache.get(mapMethodKey);    //scf方法
        if (null != method) {
            return method;
        }
        //新建method
        Class<?>[] paramTypes = new Class<?>[]{Map.class};
        if (1 != paramTypes.length) {
            //确保接入层到下游的接口是统一的
            log.error("desc=param_len_err");
            return null;
        }
        try {
            method = Class.forName(scfClass).getMethod(scfMethod, paramTypes);
            methodCache.put(mapMethodKey, method);

        } catch (Exception e) {
            log.error("desc=get_scfmethod class=" + scfClass + " method=" + scfMethod,
                    " e=" + E2s.exception2String(e));
        }
        return method;
    }
}
