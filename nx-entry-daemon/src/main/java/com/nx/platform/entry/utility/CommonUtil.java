package com.nx.platform.entry.utility;

import java.lang.reflect.Type;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.Expose;

/**
 * @author Ike
 * @declare 通用函数（偏架构功能）
 * @Created on 2016/6/1.
 */

public class CommonUtil {
    private static Gson gson = null;
    static {
        gson = buildGson();
    }
    
    public static <T> T getSafeValueFromRequest(Class<T> cla, HttpServletRequest request, String name, T defaultValue) {
        if (request == null || name == null) {
            return defaultValue;
        }
        try {
            return cla.getConstructor(String.class).newInstance(request.getParameter(name).trim());
        } catch (Exception e) {
            return defaultValue;
        }
    }
    
    public static String getLogStr(HttpServletRequest request) {
        return request.getAttribute(Constant.LOG_STRING).toString().trim();
    }
    
    public static String getLogId(HttpServletRequest request) {
        return (String)request.getAttribute(Constant.LOG_ID);
    }
    
    public static Gson getGson() {
        return gson;
    }
    
    private static Gson buildGson() {
        return new GsonBuilder().disableHtmlEscaping().registerTypeAdapter(Double.class, new JsonSerializer<Double>() {
            @Override
            public JsonElement serialize(Double src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.toString());
            }
        }).registerTypeAdapter(Long.class, new JsonSerializer<Long>() {
            @Override
            public JsonElement serialize(Long src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.toString());
            }
        }).registerTypeAdapter(Integer.class, new JsonSerializer<Integer>() {
            @Override
            public JsonElement serialize(Integer src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.toString());
            }
        }).addDeserializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                Expose expose = f.getAnnotation(Expose.class);
                return expose != null && !expose.deserialize();
            }
            
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).addSerializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                Expose expose = f.getAnnotation(Expose.class);
                return expose != null && !expose.serialize();
            }
            
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();
    }
    
    public static int calculateTotalPageCount(int pageSize, int totalCount) {
        return pageSize == 0 ? 1 : (int)Math.ceil((double)totalCount / (double)pageSize);
    }
    
}
