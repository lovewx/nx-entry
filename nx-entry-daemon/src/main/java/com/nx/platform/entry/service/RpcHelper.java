package com.nx.platform.entry.service;

import com.nx.arch.nxrpc.client.proxy.builder.ProxyFactory;
import com.nx.platform.openentry.contract.IOpenEntryConfRemote;

/**
 * @author alex
 * @brief
 * @date 2020/3/31
 */
public class RpcHelper {
    public static final IOpenEntryConfRemote openEntryConfService = ProxyFactory.create(IOpenEntryConfRemote.class, "tcp:///OpenEntryConfRemote");

}
