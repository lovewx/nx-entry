package com.nx.platform.entry.annotation.scan;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Method;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.nx.platform.entry.utility.E2s;
import com.nx.platform.openentry.annotation.ClassType;
import com.nx.platform.openentry.annotation.RPCMethod;
import com.nx.platform.openentry.annotation.RPCService;

/**
 * 注解扫描
 * Created by nx on 2019-10-09
 */
@Component
public class AnnotationScan {
    
    protected static Log log = LogFactory.getLog(AnnotationScan.class);
    
    boolean initFlag = false;
    
    private Map<String, Object> cmdMap = new HashMap<String, Object>(); // 存放配置信息
    
    private Set<String> notCheckUri = new HashSet<>(); // 不需要登录的uri
    
    // 得到配置信息
    final public Map<String, Object> getCmdMap() {
        init();
        return this.cmdMap;
    }
    
    final public Set<String> getNotCheckUri() {
        init();
        return this.notCheckUri;
    }
    
    public static final String DEFAULT_RPCMETHOD_NAME = "RPCMethod";
    
    public static final String DEFAULT_NEADLOGIN = "neadlogin";
    
    public static final String DEFAULT_RPCCLASS_NAME = "RPCClass";
    
    public static final String DEFAULT_SERVER_REG_NAME = "scfname";
    
    public static final String DEFAULT_PACKAGE_NAME = "com.nx";
    
    // 扫描注解
    public void init() {
        if (!initFlag) {
            log.warn("---------------注解扫描开始------------------");
            Set<Class<?>> classSet = getClasses(DEFAULT_PACKAGE_NAME);
            for (Class<?> classN : classSet) {
                if (classN.isAnnotationPresent(RPCService.class)) {
                    // 带有服务注解的class，扫描所有接口
                    setCmdMap(classN);
                }
            }
            initFlag = true;
        }
    }
    
    // 设置service注解信息
    final private void setCmdMap(Class<?> classN) {
        Method[] methods = classN.getMethods();
        RPCService serveAnnotation = classN.getAnnotation(RPCService.class);
        String serverRegister = serveAnnotation.scfname();
        String cmd = "";
        String methodName = "";
        for (Method method : methods) {
            if (method.isAnnotationPresent(RPCMethod.class)) {
                cmd = method.getAnnotation(RPCMethod.class).cmd();
                boolean neadLogin = method.getAnnotation(RPCMethod.class).needLogin();
                if ("".equals(cmd) || 0 == cmd.length()) {
                    // 配置cmd注解读取cmd配置,没有配置cmd注解读取方法名
                    cmd = method.getName();
                }
                
                methodName = method.getName();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty(DEFAULT_RPCCLASS_NAME, classN.getName());
                jsonObject.addProperty(DEFAULT_SERVER_REG_NAME, serverRegister);
                jsonObject.addProperty(DEFAULT_RPCMETHOD_NAME, methodName);
                jsonObject.addProperty(DEFAULT_NEADLOGIN, neadLogin);
                if (!neadLogin) {
                    notCheckUri.add(cmd);
                }
                if (cmdMap.containsKey(cmd)) {
                    throw new RuntimeException("cmd 已经存在 : " + cmd);
                } else {
                    cmdMap.put(cmd, jsonObject);
                }
            }
        }
    }
    
    /**
     * 从包package中获取所有的Class
     *
     * @param pack 包名
     * @return 包下的所有类
     */
    public static Set<Class<?>> getClasses(String pack) {
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        // 是否循环迭代
        boolean recursive = true;
        // 获取包的名字 并进行替换
        String packageName = pack;
        String packageDirName = packageName.replace(DEFAULT_POINT, DEFAULT_SLASH);
        Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            while (dirs.hasMoreElements()) {
                URL url = dirs.nextElement();
                String protocol = url.getProtocol();
                if (ClassType.file.name().equals(protocol)) {
                    System.err.println("file类型的扫描");
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    findAndAddClassesInPackageByFile(packageName, filePath, recursive, classes);
                } else if (ClassType.jar.name().equals(protocol)) {
                    System.err.println("jar类型的扫描");
                    JarFile jar;
                    try {
                        jar = ((JarURLConnection)url.openConnection()).getJarFile();
                        Enumeration<JarEntry> entries = jar.entries();
                        while (entries.hasMoreElements()) {
                            JarEntry entry = entries.nextElement();
                            String name = entry.getName();
                            if (name.charAt(0) == DEFAULT_SLASH) {
                                name = name.substring(1);
                            }
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf(DEFAULT_SLASH);
                                if (idx != -1) {
                                    packageName = name.substring(0, idx).replace(DEFAULT_SLASH, DEFAULT_POINT);
                                }
                                if ((idx != -1) || recursive) {
                                    if (name.endsWith(DEFAULT_SUFFIX_CLASS) && !entry.isDirectory()) {
                                        String className = name.substring(packageName.length() + 1, name.length() - 6);
                                        try {
                                            if (packageName.contains(DEFAULT_KEY_WORD) || packageName.contains(DEFAULT_KEY_WORD_SERVICE)) {
                                                classes.add(Class.forName(packageName + DEFAULT_POINT + className));
                                            }
                                        } catch (ClassNotFoundException e) {
                                            log.error("e=" + E2s.exception2String(e));
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error("e=" + E2s.exception2String(e));
                    }
                }
            }
        } catch (Exception e) {
            log.error("e=" + E2s.exception2String(e));
        }
        
        return classes;
    }
    
    private static final String DEFAULT_SUFFIX_CLASS = ".class";
    
    private static final char DEFAULT_POINT = '.';
    
    private static final char DEFAULT_SLASH = '/';
    
    /**
     * 以文件的形式来获取包下的所有Class
     *
     * @param packageName
     * @param packagePath
     * @param recursive
     * @param classes
     */
    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath, final boolean recursive, Set<Class<?>> classes) {
        File dir = new File(packagePath);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        File[] dirfiles = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(DEFAULT_SUFFIX_CLASS));
            }
        });
        for (File file : dirfiles) {
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + DEFAULT_POINT + file.getName(), file.getAbsolutePath(), recursive, classes);
            } else {
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    // if (packageName.contains(DEFAULT_KEY_WORD) || packageName.contains(DEFAULT_KEY_WORD_SERVICE)) {
                    if (packageName.contains(DEFAULT_PACKAGE_NAME)) {
                        classes.add(Thread.currentThread().getContextClassLoader().loadClass(packageName + DEFAULT_POINT + className));
                    }
                } catch (Exception e) {
                    log.error("e=" + E2s.exception2String(e));
                }
            }
        }
    }
    
    private static final String DEFAULT_KEY_WORD = "contract";
    
    private static final String DEFAULT_KEY_WORD_SERVICE = "Service";
}
