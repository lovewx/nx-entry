package com.nx.platform.entry.config;

import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfigChangeListener;
import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 *
 */

@Component
@Data
public class ApolloConfigItems {
    private static final Log log = LogFactory.getLog(ApolloConfigItems.class);

    //    @Value("${apollo.config.authTokenMd5Str}")
    private String authTokenMd5Str = "xxx";

    //    @Value("${apollo.config.spam_key}")
    private String spamKey = "entry_black";

    //    @Value("${apollo.config.redisAppKey}")
    private String redisAppKey = "entry";

    //    @Value("${apollo.config.zkProxyDir}")
    private String zkProxyDir = "entry_dir";

    //    @Value("${apollo.config.curatorClient}")
    private String curatorClient = "";

    //    @Value("${apollo.config.redisPassWord}")
    private String redisPassWord = "nx1009";


    @ApolloConfigChangeListener
    public void onChange(ConfigChangeEvent changeEvent) {
        log.info("Changes for namespace " + changeEvent.getNamespace());
        for (String key : changeEvent.changedKeys()) {
            ConfigChange change = changeEvent.getChange(key);
            log.info(String.format("Found change - key: %s, oldValue: %s, newValue: %s, changeType: %s", change.getPropertyName(), change.getOldValue(), change.getNewValue(), change.getChangeType()));
        }
    }
}
