package com.nx.platform.entry.utility;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 更好的打印日志
 * Created by nx on 2019/2/27.
 */
public class E2s {
    public static String exception2String(Exception ex) {
        String exceptionMessage = "";
        if (ex != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            try {
                ex.printStackTrace(pw);
                exceptionMessage = sw.toString();
            } finally {
                try {
                    sw.close();
                    pw.close();
                } catch (Exception e) {
                }
            }
        }
        return exceptionMessage;
    }
}
