package com.nx.platform.entry.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Sets;
import com.nx.arch.redis.clients.jedis.Jedis;
import com.nx.platform.entry.config.ApolloConfigItems;
import com.nx.platform.entry.utility.E2s;
import com.nx.platform.entry.utility.RedisUtil;
import com.nx.platform.entry.utility.RequestUtils;
import com.nx.platform.entry.utility.ResponseUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author alex
 * @brief
 * @date 2020/4/6
 */

@WebFilter(urlPatterns = {"/*"},
        dispatcherTypes = {DispatcherType.REQUEST},
        filterName = "filter2"
)
@Slf4j
@Component
public class AntiSpamFilter implements Filter {
    @Autowired
    ApolloConfigItems apolloConfigItems;

    private static volatile Set<String> deviceIdSet = Sets.newHashSet();
    private static volatile Set<String> ipSet = Sets.newHashSet();//从缓存内获取的封禁IP
    private static volatile Set<String> ipSet_1 = Sets.newHashSet();//从antispider服务获取的封禁IP
    private static volatile Set<String> uidSet = Sets.newHashSet();
    private static final long PERIOD = 60L * 1000;

    public void initCache() {
        /* 类加载时更新缓存 */
        deviceIdSet = updateCache("spam:deviceIds");
        ipSet = updateCache("spam:ips");
        uidSet = updateCache("spam:uids");
        /* 单线程后台定期更新缓存 */
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                deviceIdSet = updateCache("spam:deviceIds");
                ipSet = updateCache("spam:ips");
                uidSet = updateCache("spam:uids");
            }
        }, 0, PERIOD, TimeUnit.MILLISECONDS);
    }


    private Set<String> updateCache(String key) {
        String logStr = "antispiderutil update " + key;
        Jedis jedis = null;
        String redisKey = apolloConfigItems.getSpamKey() + key;
        Set<String> container = new HashSet<String>();
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            log.info(logStr, " begin key=", redisKey);
            jedis = RedisUtil.getJedis();
            if (jedis != null) {
                container = jedis.smembers(redisKey);
                log.info(logStr, " size=", container == null ? 0 : container.size());
            }
            log.info(logStr, " finish, took:" + stopwatch.toString());
        } catch (Exception ex) {
            log.error(logStr, " act=updateCache_error, ex=", E2s.exception2String(ex));
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return container;
    }

    public static Boolean isContainsDeviceId(String deviceId) {
        return !(deviceId == null || deviceId.trim().equals("")) && deviceIdSet.contains(deviceId);
    }

    public static Boolean isContainsIp(String logStr, String ip) {
        if (ip == null || ip.isEmpty()) {
            return false;
        }
        if (ipSet.contains(ip)) {
            log.info(logStr + " k=s act=entry_forbid_ip from=redis forbidIP=" + ip);
            return true;
        }
        if (ipSet_1.contains(ip)) {
            log.info(logStr + " k=s act=entry_forbid_ip from=antispider forbidIP=" + ip);
            return true;
        }
        return false;
    }

    public static Boolean isContainsUid(String uid) {
        // return !(uid == null || uid.trim().equals("")) && (uidSet.contains(uid);
        if (uid == null || uid.trim().equals("")) {
            return false;
        }
        if (uidSet.contains(uid)) {
            return true;
        }
        return false;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResp = (HttpServletResponse) servletResponse;

        String ip = RequestUtils.getRemoteAddr(httpReq);
        //获取Seesion
        Cookie[] cookies = httpReq.getCookies();
        String uid = null;
        String deviceId = "";
        if (cookies != null && cookies.length > 0) {
            log.info(" act=PpuCheckFilter.doFilter ", " cookies 为空 >>>", " <<<<");
            for (Cookie cookie : cookies) {
                if ("uid".equals(cookie.getName())) {
                    uid = cookie.getValue();
                }
                if ("deviceId".equals(cookie.getName())) {
                    deviceId = cookie.getValue();
                }
            }
        }
        //反作弊拦截
        if (isContainsIp("filter2", ip) || isContainsDeviceId(deviceId) || isContainsUid(uid)) {
            try {
                ResponseUtils.buildErrorResult(httpReq, httpResp, "身份校验失败，请重试或重新登录");
            } catch (IOException e) {
                throw e;
            }
            return;
        }
        filterChain.doFilter(httpReq, httpResp);
    }

    @Override
    public void destroy() {

    }

}
