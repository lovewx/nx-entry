package com.nx.platform.openentry.entity;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;
import lombok.Data;

import java.util.Map;

/**
 * @author
 *
 */
@SCFSerializable(name = "openetry.headerentity")
@Data
public class ServiceHeaderEntity {

    /**
     * 用户标记本次访问的唯一编号
     */
    @SCFMember(orderId = 1)
    private long logId;

    /**
     * 用于记录用户唯一标识，如果是非登录态，则填入0
     */
    @SCFMember(orderId = 2)
    private long reqUid;

    /**
     * 命令编号
     */
    @SCFMember(orderId = 3)
    private String cmd;

    /**
     * 接入层自动生成的logStr
     */
    @SCFMember(orderId = 4)
    private String logStr;

    /**
     * 服务调用链深度，接入层定义为1，每远程调用一个服务，该值就加1，如果是mq的异步调用，在mq producer也加1
     */
    @SCFMember(orderId = 5)
    private int serviceInvocationDepth;

    /**
     * 用户ip
     */
    @SCFMember(orderId = 6)
    private String clientIp;

    /**
     * token,前端设备标识
     */
    @SCFMember(orderId = 7)
    private String token;

    /**
     * cookies
     */
    @SCFMember(orderId = 8)
    private Map<String, String> cookies;

    /**
     * 维度
     */
    @SCFMember(orderId = 9)
    private String lat;

    /**
     * 经度
     */
    @SCFMember(orderId = 10)
    private String lon;

    /**
     * 版本号
     */
    @SCFMember(orderId = 11)
    private String version;

}
