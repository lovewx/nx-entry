package com.nx.platform.openentry.system;


import com.nx.arch.nxrpc.client.proxy.builder.ProxyFactory;
import com.nx.platform.openentry.contract.IOpenEntryConfRemote;

/**
 * @author alex
 * @brief  用于存储映射关系
 * @date 2019/12/3
 */
public class RpcUtil {
    public static final IOpenEntryConfRemote openEntryService = ProxyFactory.create(IOpenEntryConfRemote.class, "tcp://nxentrydao/OpenEntryConfService");
}
