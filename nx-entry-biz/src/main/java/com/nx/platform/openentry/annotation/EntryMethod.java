package com.nx.platform.openentry.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

/**
 * create by alex
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ TYPE, METHOD })
public @interface EntryMethod {
    String value() default "";//方法名
    boolean needLogin() default true;//是否允许非登录态访问，默认允许，如果配置为false，则必须是登录态
    String desc() default "";// 方法描述
    String redirectModulePath() default "";// 新服务的modulePath（用于openentry内部重定向到新服务）
}
