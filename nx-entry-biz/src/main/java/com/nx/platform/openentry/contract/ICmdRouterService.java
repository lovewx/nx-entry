package com.nx.platform.openentry.contract;

import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;
import com.nx.platform.openentry.entity.CmdRouterResponse;
import com.nx.platform.openentry.entity.ServiceHeaderEntity;

import java.util.Map;

/**
 * @author alex
 * @brief  服务端反向代理的入口方法
 * @date 2019/11/29
 */
@ServiceContract
public interface ICmdRouterService {
    @OperationContract
    CmdRouterResponse process(ServiceHeaderEntity header, Map<String, String> params) throws Exception;

}
