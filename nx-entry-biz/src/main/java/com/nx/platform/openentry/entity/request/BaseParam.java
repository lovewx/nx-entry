package com.nx.platform.openentry.entity.request;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;

import java.io.Serializable;
import java.util.Map;


/**
 * Function: request必填字段 date: 2017年12月7日 下午2:35:29 <br/>
 *
 * @author liupengfei
 */
@SCFSerializable
public class BaseParam implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @SCFMember(orderId = 100)
    protected String logString;

    @SCFMember(orderId = 101)
    protected String version;

    @SCFMember(orderId = 102)
    protected Long reqUid;

    @SCFMember(orderId = 103)
    protected Long orderId;

    @SCFMember(orderId = 104, generic = true)
    protected Map<String, String> paramMap;

    public String getLogString() {
        return logString;
    }

    public void setLogString(String logString) {
        this.logString = logString;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getReqUid() {
        return reqUid;
    }

    public void setReqUid(Long reqUid) {
        this.reqUid = reqUid;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }

    @Override
    public String toString() {
        return "BaseParam [logString=" + logString + ", version=" + version + ", reqUid=" + reqUid + ", orderId="
            + orderId + "]";
    }
}
