package com.nx.platform.openentry.entity;


import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;

@SCFSerializable
public class CookieEntity {

    @SCFMember(orderId = 1)
    private String name;

    @SCFMember(orderId = 2)
    private String value;

    @SCFMember(orderId = 3)
    private String domain;

    @SCFMember(orderId = 4)
    private int maxAge;
    
    
    public CookieEntity() {}
    
    public CookieEntity(String name, String value, String domain, int maxAge) {
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.maxAge = maxAge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    @Override
    public String toString() {
        return "CookieEntity [name=" + name + ", value=" + value + ", domain=" + domain + ", maxAge=" + maxAge + "]";
    }

}
