package com.nx.platform.openentry.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * RPC服务发现注解
 * 对应实现类
 * Created by nx on 2019/2/27.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RPCService {
        String scfname() default "logic";
}
