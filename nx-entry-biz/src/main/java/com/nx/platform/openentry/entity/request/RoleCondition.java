package com.nx.platform.openentry.entity.request;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;

import java.io.Serializable;
import java.util.Set;

/**
 * 分页请求参数
 *
 * @author 王向
 * 2018/6/14
 */
@SCFSerializable
public class RoleCondition implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @SCFMember(orderId = 1)
    private Set<Long> roleIdSet;

    @SCFMember(orderId = 2)
    private String name;

    public Set<Long> getRoleIdSet() {
        return roleIdSet;
    }

    public void setRoleIdSet(Set<Long> roleIdSet) {
        this.roleIdSet = roleIdSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}
