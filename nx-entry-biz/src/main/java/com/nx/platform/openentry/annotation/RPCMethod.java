package com.nx.platform.openentry.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * RPC 方法注解
 * Created by nx on 2019/2/27.
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RPCMethod {
        String cmd() default "";
        boolean needLogin() default false;
}
