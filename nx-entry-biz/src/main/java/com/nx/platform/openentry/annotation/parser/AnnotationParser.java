package com.nx.platform.openentry.annotation.parser;


/** 
 * @author fcl
 * @version 创建时间：2017-6-13 上午10:37:05 
 * 
 */
public interface AnnotationParser {
    void execute(Class<?> clazz);
}

