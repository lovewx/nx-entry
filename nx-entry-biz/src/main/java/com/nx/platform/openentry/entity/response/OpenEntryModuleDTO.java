package com.nx.platform.openentry.entity.response;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;
import lombok.Data;

/**
 * @author alex
 * @brief
 * @date 2019/12/2
 */

@SCFSerializable
@Data
public class OpenEntryModuleDTO {

    @SCFMember(orderId = 1)
    private Long id;

    @SCFMember(orderId = 2)
    private String moduleName;  //服务名字

    @SCFMember(orderId = 3)
    private String modulePath;   //前端调用名字

    @SCFMember(orderId = 4)
    private String moduleServiceName;

    @SCFMember(orderId = 5)
    private Long createTime;

    @SCFMember(orderId = 6)
    private Long updateTime;
}
