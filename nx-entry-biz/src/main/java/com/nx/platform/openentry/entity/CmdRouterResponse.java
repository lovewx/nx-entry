package com.nx.platform.openentry.entity;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;
import lombok.Data;

import java.util.List;

/**
 * @author alex
 * @brief
 * @date 2019/11/29
 */

@SCFSerializable
@Data
public class CmdRouterResponse {
    public static final int CONTENT_TYPE_JSON = 1;
    public static final int CONTENT_TYPE_IMAGE = 2;
    /**
     * 回填请求的 sequenceId
     */
    @SCFMember(orderId = 1)
    private long sequenceId;

    /**
     * 0：成功；-1：系统失败；404：没有对应处理单元；500：命令字处理逻辑单元执行错误
     */
    @SCFMember(orderId = 2)
    private int retCode;

    /**
     * 响应的数据，这个字段结构必须是一个json结构的字符串
     */
    @SCFMember(orderId = 3)
    private String retBody;

    /**
     * 错误日志
     */
    @SCFMember(orderId = 4)
    private String errorMsg;

    /**
     * 写入cookie的值
     */
    @SCFMember(orderId = 5)
    private List<CookieEntity> cookieValue;

    /**
     * 内容类型
     */
    @SCFMember(orderId = 6)
    private Integer contentType;

    /**
     * 流类型数据
     */
    @SCFMember(orderId = 7)
    private byte[] streamData;
}
