package com.nx.platform.openentry.system;

/**
 * @author alex
 * @brief
 * @date 2019/12/3
 */
public class ParamDefaultValue {
    public static Object getDefaultValue(Class<?> aClass) {
        if (aClass.equals(int.class)) {
            return 0;
        }
        if (aClass.equals(byte.class)) {
            return 0;
        }
        if (aClass.equals(short.class)) {
            return 0;
        }
        if (aClass.equals(long.class)) {
            return 0L;
        }
        if (aClass.equals(boolean.class)) {
            return false;
        }
        if (aClass.equals(float.class)) {
            return 0.0f;
        }
        if (aClass.equals(double.class)) {
            return 0.0d;
        }
        if (aClass.equals(char.class)) {
            return '\u0000';
        }
        return null;
    }
}
