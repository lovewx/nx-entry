package com.nx.platform.openentry.annotation;

/**
 * @类名称 ClassType.java
 * @类描述 扫描文件的两种类型
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年6月9日             
 *     ----------------------------------------------
 * </pre>
 */
public enum ClassType {
    /**
     * 文件
     */
    file,
    /**
     * jar
     */
    jar
}