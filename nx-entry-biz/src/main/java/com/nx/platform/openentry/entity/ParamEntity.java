package com.nx.platform.openentry.entity;

import com.google.common.base.MoreObjects;

/**
 * 装载参数反色的信息
 */
public class ParamEntity {
    private String paramName;
    private Class<?>  paramClass;
    private Object paramValue;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Class<?> getParamClass() {
        return paramClass;
    }

    public void setParamClass(Class<?> paramClass) {
        this.paramClass = paramClass;
    }

    public Object getParamValue() {
        return paramValue;
    }

    public void setParamValue(Object paramValue) {
        this.paramValue = paramValue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("paramName", paramName)
                .add("paramClass", paramClass)
                .add("paramValue", paramValue)
                .toString();
    }
}
