package com.nx.platform.openentry.contract;

import com.nx.arch.nxrpc.server.contract.annotation.OperationContract;
import com.nx.arch.nxrpc.server.contract.annotation.ServiceContract;
import com.nx.platform.openentry.entity.response.OpenEntryCmdDto;
import com.nx.platform.openentry.entity.response.OpenEntryModuleDTO;

import java.util.List;

/**
 * @author alex
 * @brief
 * @date 2020/3/28
 */

@ServiceContract
public interface IOpenEntryConfRemote {

    @OperationContract
    List<OpenEntryCmdDto> getAllCommandConfigure(String logstr);

    @OperationContract
    OpenEntryCmdDto getCmdByModuleCmd(String logstr, long moduleId, String cmdName);

    @OperationContract
    void updateCommandConfigure(String logstr, OpenEntryCmdDto cmdDto) throws Exception;

    @OperationContract
    OpenEntryModuleDTO getModuleConfByModuleName(String logstr, String module);

    @OperationContract
    void insertCommandConfigure(String logstr, OpenEntryCmdDto cmdDto);

    @OperationContract
    List<OpenEntryModuleDTO> getAllModuleConfigure(String logstr);

    @OperationContract
    void insertModuleConfigure(String logstr, OpenEntryModuleDTO moduleDTO);
}
