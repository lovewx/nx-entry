package com.nx.platform.openentry.entity.response;

import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;
import lombok.Data;

/**
 * @author alex
 * @brief
 * @date 2019/12/2
 */

@Data
@SCFSerializable
public class OpenEntryCmdDto {
    
    @SCFMember(orderId = 1)
    private Long id;
    
    // 用于关联服务
    @SCFMember(orderId = 2)
    private Long moduleId;
    
    @SCFMember(orderId = 3)
    private String commandName;
    
    @SCFMember(orderId = 4)
    private Integer isPublicAccess;
    
    @SCFMember(orderId = 5)
    private String methodSign;
    
    @SCFMember(orderId = 6)
    private String redirectModulePath;
    
    @SCFMember(orderId = 7)
    private Long createTime;
    
    @SCFMember(orderId = 8)
    private Long updateTime;
}
