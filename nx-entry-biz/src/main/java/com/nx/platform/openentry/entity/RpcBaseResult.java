package com.nx.platform.openentry.entity;

import com.alibaba.fastjson.JSONObject;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFMember;
import com.nx.arch.nxrpc.serializer.component.annotation.SCFSerializable;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by nx on 2017/3/29.
 */
@SCFSerializable(name = "openEntry.entity.scfBaseResult")
public class RpcBaseResult<T> {

    public static final int CONTENT_TYPE_JSON = 1;
    public static final int CONTENT_TYPE_IMAGE = 2;

    @SCFMember(orderId = 1)
    private int code;
    @SCFMember(orderId = 2)
    private String errorMsg;

    @SCFMember(orderId = 3, generic = true)
    private T data;

    /**
     * 写入cookie的值
     */
    @SCFMember(orderId = 5)
    private List<CookieEntity> cookieValue;

    /**
     * 内容类型
     */
    @SCFMember(orderId = 6)
    private Integer contentType;

    /**
     * 流类型数据
     */
    @SCFMember(orderId = 7)
    private byte[] streamData;

    @SCFMember(orderId = 8)
    private String logId;

    public boolean isSuccess() {
        return code == 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public String getErrorJsonMsg() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("errorMsg", errorMsg);
        return jsonObject.toString();
    }

    public void setErrorMsg(String errorMsg) {
        if (StringUtils.isNotBlank(errorMsg)) {
            if (code == 0) {
                this.code = -1;
            }
        }
        this.errorMsg = errorMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<CookieEntity> getCookieValue() {
        return cookieValue;
    }

    public void setCookieValue(List<CookieEntity> cookieValue) {
        this.cookieValue = cookieValue;
    }

    public Integer getContentType() {
        return contentType;
    }

    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    public byte[] getStreamData() {
        return streamData;
    }

    public void setStreamData(byte[] streamData) {
        this.streamData = streamData;
    }

    public String getLogId() {
        return logId;
    }

    public RpcBaseResult<T> setLogId(String logId) {
        this.logId = logId;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("OpenScfBaseResult [code=");
        builder.append(code);
        builder.append(", errorMsg=");
        builder.append(errorMsg);
        builder.append(", data=");
        builder.append(data);
        builder.append(", cookieValue=");
        builder.append(cookieValue);
        builder.append("]");
        return builder.toString();
    }

}
