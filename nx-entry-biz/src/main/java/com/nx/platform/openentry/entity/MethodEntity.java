package com.nx.platform.openentry.entity;

import com.google.common.base.MoreObjects;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 装载反射一个方法需要的信息
 * Created by nx on 2017/7/14.
 */
public class MethodEntity {
    private Method method;
    private Object obj;
    private List<ParamEntity> params;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public List<ParamEntity> getParams() {
        return params;
    }

    public void setParams(List<ParamEntity> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("method", method)
                .add("obj", obj)
                .add("params", params)
                .toString();
    }

}
